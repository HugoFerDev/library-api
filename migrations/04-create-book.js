module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'Book',
      require('../types/book')(Sequelize)
    )
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Book')
  },
}
