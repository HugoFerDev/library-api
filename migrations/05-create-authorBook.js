module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'AuthorBook',
      require('../types/authorBook')(Sequelize)
    )
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('AuthorBook')
  },
}
