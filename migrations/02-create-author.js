module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'Author',
      require('../types/author')(Sequelize)
    )
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Author')
  },
}
