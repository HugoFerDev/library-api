module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'Category',
      require('../types/category')(Sequelize)
    )
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Category')
  },
}
