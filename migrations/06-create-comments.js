module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'Comments',
      require('../types/comments')(Sequelize)
    )
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Comments')
  },
}
