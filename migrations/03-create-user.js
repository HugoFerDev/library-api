module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'User',
      require('../types/user')(Sequelize)
    )
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('User')
  },
}
