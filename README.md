# [ Library ] Back End NodeJS

## Requisitos

Node.js [10.16.0](https://nodejs.org/dist/v10.16.0)\
Yarn [6.4.1](https://github.com/yarnpkg/yarn/releases/tag/v1.16.0) Or\
Npm [6.4.1](https://www.npmjs.com/package/npm/v/6.4.1)

## Passos

1. Faça o clone/download deste repositório: `git clone https://hugofv@bitbucket.org/HugoFerDev/library-api.git`
2. Instale as dependências: `yarn install` or `npm install`
3. Start a aplicação: `yarn start` ou `npm start`
4. Execute os comandos a seguir.

## Comandos de Banco de dados

1. Criação do banco de dados `yarn sequelize-cli db:create`
2. Criação das tabelas do banco `yarn sequelize-cli db:migrate`
3. Criação valores padrões `yarn sequelize-cli db:seed:all`
