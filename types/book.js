module.exports = DataTypes => {
  return {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },

    name: DataTypes.STRING,
    description: DataTypes.STRING(700),
    publisher: DataTypes.STRING,
    image: DataTypes.STRING(900),
    yearRelease: DataTypes.INTEGER,

    CategoryId: {
      type: DataTypes.INTEGER,
      references: { model: 'Category', key: 'id' },
    },

    deletedAt: DataTypes.DATE,
    createdAt: { allowNull: false, type: DataTypes.DATE },
    updatedAt: { allowNull: false, type: DataTypes.DATE },
  }
}
