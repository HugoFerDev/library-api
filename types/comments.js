module.exports = DataTypes => {
  return {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    description: DataTypes.STRING(900),
    UserId: {
      type: DataTypes.INTEGER,
      references: { model: 'User', key: 'id' },
    },
    BookId: {
      type: DataTypes.INTEGER,
      references: { model: 'Book', key: 'id' },
    },

    deletedAt: DataTypes.DATE,
    createdAt: { allowNull: false, type: DataTypes.DATE },
    updatedAt: { allowNull: false, type: DataTypes.DATE },
  }
}
