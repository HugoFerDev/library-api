module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define('User', require('../types/user')(DataTypes), {
    paranoid: true,
    freezeTableName: true,
  })

  return Model
}
