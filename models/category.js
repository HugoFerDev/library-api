module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define(
    'Category',
    require('../types/category')(DataTypes),
    { paranoid: true, freezeTableName: true }
  )

  return Model
}
