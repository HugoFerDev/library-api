module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define(
    'AuthorBook',
    require('../types/authorBook')(DataTypes),
    { paranoid: true, freezeTableName: true }
  )

  Model.associate = models => {
    Model.belongsTo(models.Author, { foreignKey: { allowNull: false } })
    Model.belongsTo(models.Book, { foreignKey: { allowNull: false } })
  }

  return Model
}
