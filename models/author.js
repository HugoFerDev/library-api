module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define(
    'Author',
    require('../types/author')(DataTypes),
    { paranoid: true, freezeTableName: true }
  )

  Model.associate = models => {
    Model.belongsToMany(models.Book, { through: models.AuthorBook })
  }

  return Model
}
