module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define('Book', require('../types/book')(DataTypes), {
    paranoid: true,
    freezeTableName: true,
  })

  Model.associate = models => {
    Model.belongsTo(models.Category, { foreignKey: { allowNull: false } })
    Model.belongsToMany(models.Author, { through: models.AuthorBook })
  }

  return Model
}
