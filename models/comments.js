module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define(
    'Comments',
    require('../types/comments')(DataTypes),
    { paranoid: true, freezeTableName: true }
  )

  Model.associate = models => {
    Model.belongsTo(models.Book, { foreignKey: { allowNull: false } })
    Model.belongsTo(models.User, { foreignKey: { allowNull: false } })
  }

  return Model
}
