const express = require('express')
const models = require('./../models/index')

const router = express.Router()

router.post('/', (req, res) => {
  try {
    return models.Category.create(req.body)
      .then(data => res.status(201).send(data))
      .catch(err => {
        res.status(400).send(err)
      })
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    })
  }
})

router.put('/:id', (req, res) => {
  try {
    return models.Category.update(req.body, { where: { id: req.params.id } })
      .then(() => {
        models.Category.findByPk(req.params.id).then(data =>
          res.status(200).send(data)
        )
      })
      .catch(err => {
        res.status(400).send(err)
      })
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    })
  }
})

router.delete('/:id', (req, res) => {
  try {
    return models.Category.findByPk(req.params.id)
      .then(item => {
        item.destroy({ returning: true }).then(() => res.status(200).send())
      })
      .catch(err => {
        res.status(400).send(err)
      })
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    })
  }
})

router.get('/', (req, res) => {
  try {
    return models.Category.findAll()
      .then(data => res.status(200).send(data))
      .catch(err => {
        res.status(400).send(err)
      })
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    })
  }
})

module.exports = app => app.use('/api/category', router)
