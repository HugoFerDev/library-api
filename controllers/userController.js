const express = require('express')
const models = require('./../models/index')

const router = express.Router()

router.post('/', (req, res) => {
  try {
    return models.Category.create(req.body)
      .then(data => res.status(201).send(data))
      .catch(err => {
        res.status(400).send(err)
      })
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    })
  }
})

router.get('/', (req, res) => {
  try {
    return models.Category.findAll(req.body)
      .then(data => res.status(200).send(data))
      .catch(err => {
        res.status(400).send(err)
      })
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    })
  }
})

module.exports = app => app.use('/api/user', router)
