const express = require('express')
const models = require('./../models/index')

const router = express.Router()

router.post('/', (req, res) => {
  try {
    return models.Book.create(req.body)
      .then(book => {
        var authorBooks = req.body.Authors.map(author => ({
          AuthorId: author.id,
          BookId: book.id,
        }))
        models.AuthorBook.bulkCreate(authorBooks, { returning: true }).then(
          () => {
            models.Book.findByPk(book.id, {
              include: [{ model: models.Category }, { model: models.Author }],
            }).then(data => res.status(201).send(data))
          }
        )
      })
      .catch(err => {
        res.status(400).send(err)
      })
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    })
  }
})

router.post('/:id/comment', (req, res) => {
  try {
    var comment = {
      description: req.body.description,
      UserId: 1,
      BookId: req.params.id,
    }
    return models.Comments.create(comment)
      .then(() => {
        models.Book.findByPk(req.params.id, {
          include: [{ model: models.Category }, { model: models.Author }],
        }).then(async data => {
          data.dataValues.comments = await models.Comments.findAll({
            include: [{ model: models.User }],
            where: { BookId: data.id },
          })

          res.status(200).send(data)
        })
      })
      .catch(err => {
        res.status(400).send(err)
      })
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    })
  }
})

router.put('/:id', (req, res) => {
  try {
    return models.Book.update(req.body, { where: { id: req.params.id } })
      .then(() => {
        var authorBooks = req.body.Authors.map(author => ({
          AuthorId: author.id,
          BookId: req.params.id,
        }))

        models.AuthorBook.destroy({ where: { BookId: req.params.id } }).then(
          () => {
            models.AuthorBook.bulkCreate(authorBooks, {
              returning: true,
            }).then(() => {
              models.Book.findByPk(req.params.id, {
                include: [{ model: models.Category }, { model: models.Author }],
              }).then(async data => {
                data.dataValues.comments = await models.Comments.findAll({
                  include: [{ model: models.User }],
                  where: { BookId: data.id },
                })

                res.status(200).send(data)
              })
            })
          }
        )
      })
      .catch(err => {
        res.status(400).send(err)
      })
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    })
  }
})

router.delete('/:id', (req, res) => {
  try {
    return models.Book.findByPk(req.params.id)
      .then(item => {
        item.destroy({ returning: true }).then(() => res.status(200).send())
      })
      .catch(err => {
        res.status(400).send(err)
      })
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    })
  }
})

router.get('/', (req, res) => {
  try {
    return models.Book.findAll({
      include: [{ model: models.Category }, { model: models.Author }],
    })
      .then(async data => {
        for (let i = 0; i < data.length; i++) {
          var id = data[i].dataValues.id
          data[i].dataValues.comments = await models.Comments.findAll({
            include: [{ model: models.User }],
            where: { BookId: id },
          })
        }

        res.status(200).send(data)
      })
      .catch(err => {
        res.status(400).send(err)
      })
  } catch (err) {
    return res.status(500).send({
      error: 'Houve algum problema, Favor contactar o administrador.',
    })
  }
})

module.exports = app => app.use('/api/book', router)
